<div>

<div>

<h1 align="center">Hey there, I'm Zakaria</h1>
<h3 align="left">I'm a full-stack web developer. I finished my institute and am currently learning all there is to know about development in general, but I still specialize in web development. I'm currently learning about algorithms and data structures. Afterward, I'll learn about design patterns. Until then, I'll look for something else to learn and master!</h3>

- How to reach me **flawlesserrordev@gmail.com**

<h2 align="left">Links:</h2>

<p align="left">
<a href="https://www.linkedin.com/in/zakaria-b-835768222/" target="blank"><img alt="Static Badge" src="https://img.shields.io/badge/LINKEDIN-blue?style=for-the-badge&logo=linkedin&logoColor=blue&labelColor=black">
</a>
<a href="https://www.leetcode.com/zakariabrahmi40" target="blank"><img alt="Static Badge" src="https://img.shields.io/badge/LEETCODE-orange?style=for-the-badge&logo=leetcode&logoColor=orange&labelColor=black">
</a>
<a href="https://www.hackerrank.com/profile/zakariabrahmi40" target="blank"><img alt="Static Badge" src="https://img.shields.io/badge/HACKERRANK-%2300EA64?style=for-the-badge&logo=hackerrank&logoColor=00EA64&labelColor=black">
</a>
</p>

<h2 align="left">Languages and Tools:</h2>

<p align="left">
<img src="http://www.w3.org/html/logo/downloads/HTML5_Badge_512.png" width="50" alt="HTML"/>
<img src="https://cdn1.iconfinder.com/data/icons/logotypes/32/badge-css-3-512.png" width="52" alt="CSS"/>
<img src="https://techracho.bpsinc.jp/wp-content/uploads/2009/08/javascript.png" width="46" alt='JavaScript'/>
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/Tailwind_CSS_Logo.svg/1024px-Tailwind_CSS_Logo.svg.png?20230715030042" width="64" alt="Tailwindcss"/>
<img src="https://icons.veryicon.com/png/o/business/vscode-program-item-icon/typescript-def.png" width="64" alt="TypeScript"/>
<img src="https://ciand.net/images/ReactJs.png" width="64" alt="ReactJS"/>
<img src="https://www.cookieyes.com/wp-content/uploads/2023/03/nextjs-icon-dark-background.png" width="64" alt="NextJS"/>
<img src="https://i0.wp.com/programmingwithmosh.com/wp-content/uploads/2020/02/reduxlogo.png?ssl=1" width="64" alt="ReduxJS"/>
<img src="https://www.ictdemy.com/images/5728/nodejs_logo.png" width="64" alt="NodeJS"/>
<img src="https://ajeetchaulagain.com/static/7cb4af597964b0911fe71cb2f8148d64/87351/express-js.png" width="64" alt="ExpressJS"/>
<img src="https://cdn.freebiesupply.com/logos/large/2x/git-icon-logo-png-transparent.png" width="64" alt="git"/>
<img src="https://www.w3schools.in/wp-content/uploads/mongodb-logo.png" width="64" alt="Mongodb"/>
<img src="https://pngimg.com/uploads/mysql/mysql_PNG23.png" width="64" alt="Mysql"/>
</p>
</div>

</div>

